<?php

namespace rednucleus\Emailsender;

use PHPMailer\PHPMailer\OAuth;
use PHPMailer\PHPMailer\PHPMailer;
use Greew\OAuth2\Client\Provider\Azure;

class SMTPEmail
{

    // PHPMAILER for sending email
    private $tenantId = null;
    private $clientID = null;
    private $clientSecret = null;
    private $refreshToken = null;
    private $mail = null;
    private $provider = null;
    private $from = null;

    public function __construct($clientID, $clientSecret, $tenantId, $refreshToken, $from)
    {
        $this->clientID = $clientID;
        $this->clientSecret = $clientSecret;
        $this->refreshToken = $refreshToken;
        $this->tenantId = $tenantId;
        $this->mail = new PHPMailer();
        $this->mail->isSMTP();
        $this->mail->Host = 'smtp.office365.com';
        $this->mail->Port = 587;
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
        $this->mail->SMTPAuth = true;
        $this->mail->AuthType = 'XOAUTH2';
        $this->from = $from;
        $this->provider = new Azure(
            [
                'clientId' => $this->clientID,
                'clientSecret' => $this->clientSecret,
                'tenantId' => $this->tenantId,
            ]
        );
        $this->mail->setOAuth(
            new OAuth(
                [
                    'provider' => $this->provider,
                    'clientId' => $this->clientID,
                    'clientSecret' => $this->clientSecret,
                    'refreshToken' => $this->refreshToken,
                    'userName' => $this->from,
                ]
            )
        );


    }

    public function send($to, $subject, $body)
    {
        $this->mail->setFrom($this->from, '');
        $this->mail->addAddress($to, '');
        $this->mail->Subject = $subject;
        $this->mail->CharSet = PHPMailer::CHARSET_UTF8;
        $this->mail->msgHTML($body, __DIR__);
        if (!$this->mail->send()) {
            echo 'Mailer Error: ' . $this->mail->ErrorInfo; // throw an exception
        }

    }


}