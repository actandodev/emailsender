<?php

namespace rednucleus\Emailsender;

use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractTransport;
use Symfony\Component\Mime\MessageConverter;

class RNSenderEmailTransport extends AbstractTransport
{
    protected $senderClass = null;
    protected $config = null;
    protected $sender = null;

    public function __construct($config)
    {
        parent::__construct();
        $this->sender = $config['sender'];
        $this->config = $config;
    }

    protected function doSend(SentMessage $message): void
    {
        $email = MessageConverter::toEmail($message->getOriginalMessage());
        $result = [
            'from_email' => $email->getFrom(),
            'to' => collect($email->getTo())->map(function ($email) {
                return ['email' => $email->getAddress(), 'type' => 'to'];
            })->all(),
            'subject' => $email->getSubject(),
            'text' => $email->getTextBody(),
            'html' => $email->getHtmlBody(),
        ];
        switch ($this->sender) {
            case 'smtp':
                $this->senderClass = new SMTPEmail(
                    $this->config['clientid'],
                    $this->config['clientsecret'],
                    $this->config['tenantid'],
                    $this->config['refreshtoken'],
                    $email->getFrom()[0]->getAddress()
                );
                break;
            case 'msgraph':
                $this->senderClass = new MicrosoftGraphEmail(
                    $this->config['clientid'],
                    $this->config['clientsecret'],
                    $this->config['tenantid'],
                    $this->config['refreshtoken'],
                    $this->config['cache']
                );
                break;
            default:
                exit();
        }

        $this->senderClass->send(
            $result['to'][0]["email"],
            $result['subject'],
            $result['text'] == null ? $result['html'] : $result['text']
        );

    }

    /**
     * Get the string representation of the transport.
     *
     * @return string
     */
    public function __toString(): string
    {
        return '';
    }

}