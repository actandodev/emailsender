# **rednucleus/emailsender Laravel Package**

rednucleus/emailsender is a Laravel package that provides a simple way to send emails using either Microsoft Graph or SMTP. Follow the steps below to install and configure the package in your Laravel project.

## **Installation**

To install the package, run the following command in your terminal:

```php
composer require rednucleus/emailsender
```

If you have some issues with dependencies you can run this command instead:

```php
composer require rednucleus/emailsender --with-all-dependencies
```


## **Configuration**

1. Set your Microsoft Graph or SMTP authentication keys in your **`.env`** file:

```
MS_CLIENT_ID=<Your MS client ID>
MS_CLIENT_SECRET=<Your MS client secret>
MS_TENANT_ID=<Your MS tenant ID>
MS_REFRESH_TOKEN=<Your MS refresh token>
MS_SENDER_TYPE=<Type of sender, either 'smtp' or 'msgraph'>
MS_CACHE=<Type of sender, either 'false' or 'true'>
```

You can enable token manager caching by setting the **`MS_CACHE`** environment variable to **`true`**. Please note that the caching feature is only available for **`msgraph`**.

1. Publish the configuration file by running the following command in your terminal:

```php
php artisan vendor:publish --provider="rednucleus\Emailsender\RNSenderEmailServiceProvider"
```

This will create an **`emailsender.php`** file in your **`config`** directory.

1. Configure the mailer by adding the following mailer configuration to your **`config/mail.php`** file:

```php
'rnemailsender' => [
    'transport' => 'rnemailsender',
    'sender' => env('MS_SENDER_TYPE', 'smtp'),
    'cache' => env('MS_CACHE', false),
],
```


## **Usage**

To use the rednucleus/emailsender mailer in your application, follow these steps:

1. Create a new mailable using the **`make:mail`** Artisan command:

```php
php artisan make:mail ExampleMail
```

1. Update the **`build`** method in your new **`ExampleMail`** class to include the content and subject of your email:

```php
public function build()
{
    return $this->subject('Example Subject')
                ->view('emails.example');
}
```

1. In your application code, use the **`Mail`** facade to send the email using the **`rnemailsender`** mailer:

```php
use Illuminate\Support\Facades\Mail;

Mail::to('example@example.com')->send(new ExampleMail());

```

And that's it! You should now be able to send emails using the rednucleus/emailsender package in your Laravel application.